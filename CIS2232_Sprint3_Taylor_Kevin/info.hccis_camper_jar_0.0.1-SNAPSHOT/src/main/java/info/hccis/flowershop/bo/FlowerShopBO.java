/**
 * @author K.Taylor
 * Date:11/03/2020
 * Subject: CIS-2232 Flower Shop (Sprint 3)
 * Instructor: BJ MacLean
 * 
 * 
 * Flower Shop Business Object
 */

package info.hccis.flowershop.bo;

import info.hccis.flowershop.jpa.entity.FlowerShop;
import info.hccis.flowershop.dao.FlowerShopDAO;
import java.util.ArrayList;


public class FlowerShopBO {

    /**
     * Select all records from the database
     *
     * @return List of the customers from the database
     * @author K.Taylor
     */
    public ArrayList<FlowerShop> selectAll() {

        //Read from the database
        FlowerShopDAO flowerShopDAO = new FlowerShopDAO();
        ArrayList<FlowerShop> flowerShopList = flowerShopDAO.loadAll();
        return flowerShopList;
    }

    
    
    
    public ArrayList<FlowerShop> showCustomer() {

        //Read from the database
        FlowerShopDAO flowerShopDAO = new FlowerShopDAO();
        
        //Searchs for the customer information from the database
        ArrayList<FlowerShop> flowerShopList = flowerShopDAO.search();
        return flowerShopList;
    }

    
    
    /**
     * Update the customer information into the database
     *
     * @author K.Taylor
     */
    public boolean update(FlowerShop flowerShop) {
        FlowerShopDAO flowerShopDAO = new FlowerShopDAO();
        return flowerShopDAO.update(flowerShop);
    }

    public FlowerShop save(FlowerShop flowerShop) {
      FlowerShopDAO flowerShopDAO = new FlowerShopDAO();

        if (flowerShop.getId() == 0) {
            flowerShop = flowerShopDAO.insert(flowerShop);
        } else {
            flowerShopDAO.update(flowerShop);
        }

        return flowerShop;
    }

    /**
     * Delete the customer record
     *
     * @author K.Taylor
     */
    public boolean delete(int id) {
        FlowerShopDAO flowerShopDAO = new FlowerShopDAO();
        return flowerShopDAO.delete(id);
    }

}
