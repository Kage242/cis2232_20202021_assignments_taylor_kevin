/**
 * @author K.Taylor
 * Date:11/03/2020
 * Subject: CIS-2232 Flower Shop (Sprint 3)
 * Instructor: BJ MacLean
 *
 *
 * Database entity class
 */
package info.hccis.flowershop.jpa.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Represents a flower shop customer entity
 *
 * @author K.Taylor
 */
public class FlowerShop implements Serializable {

    /**
     * @return the flowerOrder
     */
    public String getFlowerOrder() {
        return flowerOrder;
    }

    /**
     * @param flowerOrder the flowerOrder to set
     */
    public void setFlowerOrder(String flowerOrder) {
        this.flowerOrder = flowerOrder;
    }

    /**
     * @return the customerTypeDescription
     */
    public String getCustomerTypeDescription() {
        return customerTypeDescription;
    }

    /**
     * @param customerTypeDescription the customerTypeDescription to set
     */
    public void setCustomerTypeDescription(String customerTypeDescription) {
        this.customerTypeDescription = customerTypeDescription;
    }

    public FlowerShop(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the fullName
     */
    public String getFullName() {
        return fullName;
    }

    /**
     * @param fullName the fullName to set
     */
    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    /**
     * @return the address
     */
    public String getAddress() {
        return address;
    }

    /**
     * @param address the address to set
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * @return the city
     */
    public String getCity() {
        return city;
    }

    /**
     * @param city the city to set
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     * @return the province
     */
    public String getProvince() {
        return province;
    }

    /**
     * @param province the province to set
     */
    public void setProvince(String province) {
        this.province = province;
    }

    /**
     * @return the postalCode
     */
    public String getPostalCode() {
        return postalCode;
    }

    /**
     * @param postalCode the postalCode to set
     */
    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    /**
     * @return the phoneNumber
     */
    public String getPhoneNumber() {
        return phoneNumber;
    }

    /**
     * @param phoneNumber the phoneNumber to set
     */
    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    /**
     * @return the birthDate
     */
    public String getBirthDate() {
        return birthDate;
    }

    /**
     * @param birthDate the birthDate to set
     */
    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    /**
     * @return the loyaltyCard
     */
    public String getLoyaltyCard() {
        return loyaltyCard;
    }

    /**
     * @param loyaltyCard the loyaltyCard to set
     */
    public void setLoyaltyCard(String loyaltyCard) {
        this.loyaltyCard = loyaltyCard;
    }

    /**
     * @return the customerTypeId
     */
    public int getCustomerTypeId() {
        return customerTypeId;
    }

    /**
     * @param customerTypeId the customerTypeId to set
     */
    public void setCustomerTypeId(int customerTypeId) {
        this.customerTypeId = customerTypeId;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private static final long serialVersionUID = 1L;
    private int id;

    @Basic(optional = false)
    @Column(name = "customerTypeId")
    private int customerTypeId;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "fullName")
    private String fullName;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "address1")
    private String address;

    @Basic(optional = false)
    @NotNull
    @Size(min = 3, max = 6)
    @Column(name = "city")
    private String city;

    @Basic(optional = false)
    @NotNull
    @Size(min = 3, max = 140)
    @Column(name = "province")
    private String province;

    @Basic(optional = false)
    @NotNull
    @Size(min = 3, max = 140)
    @Column(name = "postalCode")
    private String postalCode;

    @Basic(optional = false)
    @NotNull
    @Size(min = 3, max = 140)
    @Column(name = "phoneNumber")
    private String phoneNumber;
    @Basic(optional = false)
    @NotNull
    @Size(min = 3, max = 140)
    @Column(name = "birthDate")
    private String birthDate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 3, max = 140)
    @Column(name = "loyaltyCard")
    private String loyaltyCard;

    @Transient
    private String customerTypeDescription;
    @Transient
    private String flowerOrder;

    @Transient
    private String itemType;

    @Transient
    private String orderStatus;

    public FlowerShop() {
    }

    public FlowerShop(int id, int customerTypeId, String fullName, String address, String city, String province, String postalCode, String phoneNumber, String birthDate, String loyaltyCard) {
        this.id = id;
        this.customerTypeId = customerTypeId;
        this.fullName = fullName;
        this.address = address;
        this.city = city;
        this.province = province;
        this.postalCode = postalCode;
        this.phoneNumber = phoneNumber;
        this.birthDate = birthDate;
        this.loyaltyCard = loyaltyCard;

    }

    public void display() {
        System.out.println(this.toString());
    }

    public String toString() {
        return id + ") Name: " + fullName + " " + "Address: " + address + " City: " + city
                + "Province: " + province + "Postal Code: " + postalCode + "Phone Number: " + phoneNumber
                + "Birth Date: " + birthDate + "Loyalty Card: " + loyaltyCard;
    }

    /**
     * @return the itemType
     */
    public String getItemType() {
        return itemType;
    }

    /**
     * @param itemType the itemType to set
     */
    public void setItemType(String itemType) {
        this.itemType = itemType;
    }

    /**
     * @return the orderStatus
     */
    public String getOrderStatus() {
        return orderStatus;
    }

    /**
     * @param orderStatus the orderStatus to set
     */
    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

}
