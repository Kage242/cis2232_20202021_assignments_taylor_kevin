package info.hccis.flowershop.entity;

/**
 * Used to hold attributes needed for a camper reort.
 * @author bjm
 * @since 4-Nov-2020
 */
public class OrderSummary {

    private String dateOfBirth;

    //Can add additional attributes in here if needed.
    
    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }
    
    
    
}
