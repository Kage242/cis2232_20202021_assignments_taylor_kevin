/**
 * @author K.Taylor
 * Date:11/03/2020
 * Subject: CIS-2232 Flower Shop (Sprint 3)
 * Instructor: BJ MacLean
 *
 * Flower Shop customer type database access object
 */
package info.hccis.flowershop.dao;

import info.hccis.flowershop.jpa.entity.CustomerType;
import info.hccis.flowershop.util.DatabaseUtility;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author K.Taylor
 */
public class CustomerTypeDAO {

    public ArrayList<CustomerType> select() {
        ArrayList<CustomerType> customerTypes = new ArrayList();

        Connection conn = null;
        try {

            //Select the campers from the database
            conn = DatabaseUtility.getConnection("");
            Statement statement = conn.createStatement();

            //***************************************************
            // Select using statement
            //***************************************************
            //Next select all the rows and display them here...
            ResultSet rs = statement.executeQuery("select * from customertype");

            //Show all the campers
            while (rs.next()) {
                int id = rs.getInt("id");
                String description = rs.getString("description");

                CustomerType customerType = new CustomerType();
                customerType.setId(id);
                customerType.setDescription(description);
                customerTypes.add(customerType);
            }

        } catch (SQLException ex) {
            Logger.getLogger(CustomerTypeDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                conn.close();
            } catch (SQLException ex) {
                //Could not close.  
                System.out.println("Error closing the connection.");
            }
        }
        return customerTypes;

    }

//public ArrayList<CustomerType> search() {
//        ArrayList<CustomerType> customerTypes = new ArrayList();
//
//        Connection conn = null;
//        try {
//
//            //Select the campers from the database
//            conn = DatabaseUtility.getConnection("");
//            Statement statement = conn.createStatement();
//
//            //***************************************************
//            // Select using statement
//            //***************************************************
//            //Next select all the rows and display them here...
//            ResultSet rs = statement.executeQuery("select * from customer, customertype, flowerorder, itemtype, orderstatustype "
//              );
//
//            //Show all the campers
//            while (rs.next()) {
//                int id = rs.getInt("id");
//                String description= rs.getString("description");
//               
//
//                CustomerType customerType = new CustomerType();
//                customerType.setId(id);
//                customerType.setDescription(description);
//                customerTypes.add(customerType);
//            }
//
//        } catch (SQLException ex) {
//            Logger.getLogger(CustomerTypeDAO.class.getName()).log(Level.SEVERE, null, ex);
//        } finally {
//            try {
//                conn.close();
//            } catch (SQLException ex) {
//                //Could not close.  
//                System.out.println("Error closing the connection.");
//            }
//        }
//        return customerTypes;
//
//    }
}
