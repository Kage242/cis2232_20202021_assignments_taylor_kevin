/**
 * @author K.Taylor
 * Date:11/03/2020
 * Subject: CIS-2232 Flower Shop (Sprint 3)
 * Instructor: BJ MacLean
 */


package info.hccis.customer.repositories;

import info.hccis.flowershop.jpa.entity.Customer;
import java.util.ArrayList;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomerRepository extends CrudRepository<Customer, Integer> {
    
    ArrayList<Customer> findAllByName(String fullName);
    //ArrayList<Customer> findAllByLastName(String lastName); //created but not used in project yet.
    
}