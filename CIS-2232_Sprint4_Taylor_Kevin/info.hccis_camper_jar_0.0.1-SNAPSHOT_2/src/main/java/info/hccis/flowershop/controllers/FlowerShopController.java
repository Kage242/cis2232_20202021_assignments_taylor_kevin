/**
 * @author K.Taylor
 * Date:11/03/2020
 * Subject: CIS-2232 Flower Shop (Sprint 3)
 * Instructor: BJ MacLean
 *
 *
 * Flower Shop Controller class
 */
package info.hccis.flowershop.controllers;

import info.hccis.flowershop.bo.FlowerShopBO;
import info.hccis.flowershop.jpa.entity.FlowerShop;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Controller for the Flower shop functionality of the site
 *
 * @author K.Taylor
 */
@Controller
@RequestMapping("/flowerShop")
public class FlowerShopController {
//
//     private final CustomerRepository customerRepository;
//
//    public FlowerShopController(CustomerRepository cr) {
//        customerRepository = cr;
//    }

    /**
     * Page to allow user to view customers
     *
     * @since 20201019
     * @author K.Taylor
     */
    @RequestMapping("/list")
    public String list(Model model) {

        //Get the customers from the database.
        FlowerShopBO flowerShopBO = new FlowerShopBO();
        ArrayList<FlowerShop> flowerShopList = flowerShopBO.selectAll();

        model.addAttribute("flowerShopList", flowerShopList);
        model.addAttribute("findNameMessage", "Customers loaded");

        return "flowerShop/list";
    }

    /**
     * Page to allow user to add customer information
     *
     * @author K.Taylor
     */
    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public String add(Model model) {

        model.addAttribute("message", "Add new customer");

        FlowerShop flowerShop = new FlowerShop();
        model.addAttribute("flowerShop", flowerShop);

        return "flowerShop/add";
    }

    /**
     * Page to allow user to submit the new customer. It will put the customer
     * record in the database.
     *
     * @author K.Taylor
     */
    @RequestMapping("/addSubmit")
    public String addSubmit(Model model, @Valid @ModelAttribute("flowerShop") FlowerShop flowerShop, BindingResult result) {

        //If errors put the object back in model and send back to the add page.
        if (result.hasErrors()) {
            System.out.println("Validation error");
            return "flowerShop/add";
        }

        //Save that customer to the database
        FlowerShopBO flowerShopBO = new FlowerShopBO();
        flowerShopBO.save(flowerShop);
        
        //Saves information from customer table to JSON file
        flowerShopBO.saveCustomer();

        //reload the list
        ArrayList<FlowerShop> flowerShopList = flowerShopBO.selectAll();

        String propFileName = "messages";
        ResourceBundle rb = ResourceBundle.getBundle(propFileName);
        String successAddString = rb.getString("message.customer.saved");

        model.addAttribute("message", successAddString);
        model.addAttribute("flowerShopList", flowerShopList);

        return "flowerShop/list";

    }

    /**
     * Page to allow user to edit a customer record
     *
     * @author K.Taylor
     */
    @RequestMapping("/edit")
    public String edit(Model model, HttpServletRequest request) {

        String idString = (String) request.getParameter("id");
        int id = Integer.parseInt(idString);
        System.out.println("BJTEST - id=" + id);

        //reload the list
        FlowerShopBO flowerShopBO = new FlowerShopBO();
        ArrayList<FlowerShop> flowerShopList = flowerShopBO.selectAll();

        FlowerShop found = null;
        for (FlowerShop current : flowerShopList) {
            if (current.getId() == id) {
                found = current;
                break;
            }
        }

        model.addAttribute("flowerShop", found);
        return "flowerShop/add";
    }

    /**
     * Page to allow user to delete a customer record
     *
     * @since 20201019
     * @author K.Taylor
     */
    @RequestMapping("/delete")
    public String delete(Model model, HttpServletRequest request) {

        String idString = (String) request.getParameter("id");
        int id = Integer.parseInt(idString);
        System.out.println("id= " + id);

        //reload the list
        FlowerShopBO flowerShopBO = new FlowerShopBO();
        boolean deleted = flowerShopBO.delete(id);

        String propFileName = "messages";
        ResourceBundle rb = ResourceBundle.getBundle(propFileName);

        String successString;
        if (deleted) {
            successString = rb.getString("message.customer.deleted") + " (" + id + ")";
        } else {
            successString = rb.getString("message.customer.deleted.error") + " (" + id + ")";
        }
        ArrayList<FlowerShop> flowerShopList = flowerShopBO.selectAll();

        model.addAttribute("message", successString);
        model.addAttribute("flowerShopList", flowerShopList);

        return "flowerShop/list";
    }

}
