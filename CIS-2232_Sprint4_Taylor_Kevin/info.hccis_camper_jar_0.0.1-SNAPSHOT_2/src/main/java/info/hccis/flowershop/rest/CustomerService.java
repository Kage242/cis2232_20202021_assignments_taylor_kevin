/**
 * @author K.Taylor
 * Date:11/16/2020
 * Subject: CIS-2232 Flower Shop - Web Services
 * Instructor: BJ MacLean
 *
 *
 * Flower Shop REST Service class
 */
package info.hccis.flowershop.rest;

import com.google.gson.Gson;
import info.hccis.flowershop.repositories.CustomerRepository;
import info.hccis.flowershop.bo.FlowerShopBO;
import info.hccis.flowershop.jpa.entity.Customer;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Optional;
import javax.ws.rs.Consumes;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.springframework.beans.factory.annotation.Autowired;


@Path("/CustomerService/customers")
public class CustomerService {

    private final CustomerRepository cr;

    @Autowired
  
    public CustomerService(CustomerRepository cr) {
        this.cr = cr;
    }

    /**
     * Converts the customer data to JSON file
     *
     * @return
     * @author: K.Taylor
     * @since 20201116
     */
    @GET
    @Produces("application/json")
    public ArrayList<Customer> getAll() {
        ArrayList<Customer> customers = (ArrayList<Customer>) cr.findAll();
        return customers;
    }

    
     /**
     * Returns the customer data from the database based on id
     *
     * @return
     * @author: K.Taylor
     * @since 20201116
     */
   @GET
   @Path("/{id}")
   @Produces("application/json")
    public Response getCustomerById(@PathParam("id") int id) throws URISyntaxException {
        Optional<Customer> customer = cr.findById(id);

      if (!customer.isPresent()) {
           return Response.status(204).build();
       } else {
            return Response
                   .status(200)
                   .entity(customer).build();
        }
    }
    /**
     * Adds a new customer to the JSON file
     *
     * @param customerJson
     * @return
     * @author: K.Taylor
     * @since 20201116
     *
     */
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createCustomer(String customerJson) {

       FlowerShopBO flowerShopBO = new FlowerShopBO();
        
        Gson gson = new Gson();
        Customer customer = gson.fromJson(customerJson, Customer.class);

        if (customer.getFullName() == null || customer.getFullName().isEmpty()) {
            return Response.status(400).entity("Please provide all mandatory inputs").build();
        }

        if (customer.getId() == null) {
            customer.setId(0);
        }

        customer = cr.save(customer);
        
        
        //Saves the customer data to the JSON file created in prior steps
        flowerShopBO.saveCustomer();
       
       String temp ="";
        temp = gson.toJson(customer);
       
       
        return Response.status(201).entity(temp).header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").build();
    }
//    
//    @DELETE
//    @Path("/{id}")
//    public Response deleteBooking(@PathParam("id") int id) throws URISyntaxException {
//        Optional<Booking> booking = br.findById(id);
//        if(booking != null) {
//            br.deleteById(id);
//            return Response.status(HttpURLConnection.HTTP_CREATED).build();
//        }
//        return Response.status(404).build();
//    }
//
//    @PUT
//    @Path("/{id}")
//    @Consumes("application/json")
//    @Produces("application/json")
//    public Response updateBooking(@PathParam("id") int id, String bookingJson) throws URISyntaxException 
//    {
//
//        Gson gson = new Gson();
//        Booking booking = gson.fromJson(bookingJson, Booking.class);
//        
//        if(booking.getName1() == null || booking.getName1().isEmpty()) {
//            return Response.status(400).entity("Please provide all mandatory inputs").build();
//        }
// 
//        if(booking.getId() == null){
//            booking.setId(0);
//        }
//
//        booking = br.save(booking);
////        BookingDAO bookingDAO = new BookingDAO();
////        bookingDAO.insert(booking);
//
//        String temp = "";
//        temp = gson.toJson(booking);
//
//        return Response.status(201).entity(temp).header("Access-Control-Allow-Origin", "*")
//                .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").build();        
//    }    

}
