/**
 * @author K.Taylor
 * Date:11/17/2020
 * Subject: CIS-2232 Flower Shop - Web Services
 * Instructor: BJ MacLean
 *
 *
 * Flower Shop Jersey Config class
 */


package info.hccis.flowershop;

import info.hccis.flowershop.rest.CustomerService;
import javax.annotation.PostConstruct;
import javax.ws.rs.ApplicationPath;
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.stereotype.Component;

@Component
@ApplicationPath("api")
public class JerseyConfig extends ResourceConfig {

    @PostConstruct
    private void init() {
        registerClasses(CustomerService.class);
    }
}
