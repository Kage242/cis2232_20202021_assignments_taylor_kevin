
import java.io.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Author: Kevin Taylor 
 * Date: 09/29/2020 
 * Subject: CIS-2232 Assignment 2
 * Instructor: BJ MacLean
 *
 * This class stores variables and methods used within the application
 */
public class studentInfo {

    /**
     * Variables used
     */
    private String studentName;
    private int studentID;
    private String reflectionTxt;
    private String reflectionTxt2;
    String userChoice;
    //String FILE_LOCATION = "cis2232" + File.separator + "reflection.json";
    Scanner input = new Scanner(System.in);

    /**
     * This method creates a file and adds the new student to the file.
     */
    public void addStudent() {

        try {
            BufferedWriter bufferWrite = new BufferedWriter(new FileWriter("reflections.txt", true));
            PrintWriter printWrite = new PrintWriter(bufferWrite);

            /**
             * This prompts the user for their info
             */
            System.out.println("What is your name");
            setStudentName(input.nextLine());
            System.out.println("What is your reflection text?");
            setReflectionTxt(input.nextLine());
            System.out.println("Choice:  (U-Update reflection, Enter to skip)");
            userChoice = input.nextLine();
            System.out.println("What is your student number?");
            setStudentID(input.nextInt());

            if (userChoice == "U") {
                System.out.println("Additional reflection: ");
                setReflectionTxt2(input.nextLine());
            } else {
                setReflectionTxt2("");
            }
            /**
             * This displays a message to let the user know their info has been
             * created and saved
             */

            System.out.println("New Student Created! \n" + "About to save: " + "\n" + getStudentName() + "\n" + getStudentID() + "\n" + getReflectionTxt() + "\n" + getReflectionTxt2());
            printWrite.println(getStudentName());
            printWrite.println(getStudentID());
            printWrite.println(getReflectionTxt());
            printWrite.println(getReflectionTxt2());
            printWrite.close();
            connectDB();
            saveStudents();

            System.out.println("Student Saved!");

        } catch (IOException e) {
            System.out.println("Error! File doesn't exist");
        }

    }

    /**
     * This method creates a connection to the database
     *
     * @return
     */
    public Connection connectDB() {
        Connection conn = null;

        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            conn = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/ojt",
                    "root",
                    "");
            System.out.println("Connection established!");
            conn.close();

        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | SQLException e) {
            System.out.println("Could not make a connection to the database");

        }
        return conn;

    }

    /**
     * This method inserts the student's information into the database after its
     * entered
     */
    private void saveStudents() {

        String insertStudent = "INSERT INTO ojtreflection (studentId , studentName, reflection)) " + "VALUES(?, ?, ?)";

        try {
            Connection conn2 = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/ojt",
                    "root",
                    "");

            System.out.println("Inserting data.....");

            PreparedStatement pstat = conn2.prepareStatement(insertStudent);
            // Statement stmt = conn2.createStatement();

            //stmt.executeUpdate(sql);
            pstat.setInt(1, getStudentID());
            pstat.setString(2, getStudentName());
            pstat.setString(3, getReflectionTxt());
            pstat.setString(3, getReflectionTxt2());

            pstat.executeUpdate();

            System.out.println("Data inserted! Closing connecting");
            conn2.close();
        } catch (SQLException e) {
            System.out.println("Could not save student to the database");

        }

    }

    /**
     * This method displays all the students that have been added to the
     * database
     */
    public void showStudents() {

        try {
            Statement stmt = connectDB().createStatement();
            ResultSet rs = stmt.executeQuery("select * from ojtreflection");

            //Show all the students
            while (rs.next()) {
                int studentId = rs.getInt("studentId");
                String studentName = rs.getString("studentName");
                String reflection = rs.getString("reflection");
                String reflection2 = rs.getString("reflection");

                System.out.println(studentId + " , " + studentName + " , " + reflection + " , " + reflection2);
                //Camper camper = new Camper(String.valueOf(id), firstName, lastName, dob);
                // campers.add(camper);

            }

        } catch (Exception e) {
            System.out.println("Failed to view students");
        }
        try {
            FileInputStream fileStream = new FileInputStream("reflections.txt");
            DataInputStream dataStream = new DataInputStream(fileStream);

            BufferedReader br = new BufferedReader(new InputStreamReader(dataStream));

            String results;
            while ((results = br.readLine()) != null) {
                System.out.println(results + "\n");

            }
            dataStream.close();

        } catch (IOException e2) {
            System.out.println("Error! Failed to load file");
        }
        //Saves students to file
        try {
            FileInputStream fileStream = new FileInputStream("reflections.txt");
            DataInputStream dataStream = new DataInputStream(fileStream);

            BufferedReader br = new BufferedReader(new InputStreamReader(dataStream));

            String results;
            while ((results = br.readLine()) != null) {
                System.out.println(results + "\n");

            }
            dataStream.close();

        } catch (IOException e2) {
            System.out.println("Error! Failed to load file");
        }

    }

    /**
     * @return the studentName
     */
    public String getStudentName() {
        return studentName;
    }

    /**
     * @return the studentID
     */
    public int getStudentID() {
        return studentID;
    }

    /**
     * @return the reflectionTxt2
     */
    public String getReflectionTxt2() {
        return reflectionTxt2;
    }

    /**
     * @param studentName the studentName to set
     */
    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    /**
     * @param studentID the studentID to set
     */
    public void setStudentID(int studentID) {
        this.studentID = studentID;
    }

    /**
     * @param reflectionTxt2 the reflectionTxt2 to set
     */
    public void setReflectionTxt2(String reflectionTxt2) {
        this.reflectionTxt2 = reflectionTxt2;
    }

    /**
     * @return the reflectionTxt
     */
    public String getReflectionTxt() {
        return reflectionTxt;
    }

    /**
     * @param reflectionTxt the reflectionTxt to set
     */
    public void setReflectionTxt(String reflectionTxt) {
        this.reflectionTxt = reflectionTxt;
    }
}
