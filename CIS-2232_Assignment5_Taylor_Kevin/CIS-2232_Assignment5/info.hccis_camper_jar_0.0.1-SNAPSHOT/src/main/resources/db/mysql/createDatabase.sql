DROP DATABASE if exists cis2232_parking;
CREATE DATABASE cis2232_parking;
USE cis2232_parking;

CREATE TABLE ParkingPass(
id int(5) NOT NULL PRIMARY KEY AUTO_INCREMENT,
firstName varchar(20) NOT NULL,
lastName varchar(20) NOT NULL,
licensePlate varchar(6) NOT NULL,
program varchar(140) NOT NULL);
