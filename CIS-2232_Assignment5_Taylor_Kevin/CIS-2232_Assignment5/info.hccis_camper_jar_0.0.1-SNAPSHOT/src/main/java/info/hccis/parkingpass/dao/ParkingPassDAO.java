/**
 * @author K.Taylor
 * Date:11/16/2020
 * Subject: CIS-2232 Assignment 5 - Web Services
 * Instructor: BJ MacLean
 *
 *
 * Parking Pass DAO class
 */



package info.hccis.parkingpass.dao;

import info.hccis.parkingpass.jpa.entity.Parkingpass;
import info.hccis.parkingpass.jpa.entity.Parkingpass;
import info.hccis.parkingpass.util.DatabaseUtility;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * ParkingPass database access object
 *
 * @author K.Taylor
 * @since 2020116
 */
public class ParkingPassDAO {

    public static boolean checkConnection() {

        Connection conn = null;
        try {
            conn = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/cis2232_parking",
                    "root",
                    ""); //--No password.  For assignment submission this would be expected to be empty.
        } catch (Exception e) {
            return false;
        }

        return true;

    }

    /**
     * Select the student records from the database
     *
     * @since 20201116
     * @author K.Taylor
     */
    public ArrayList<Parkingpass> selectAll() {
        ArrayList<Parkingpass> parkingPasses = new ArrayList();

        //Select from the database
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/cis2232_parking",
                    "root",
                    ""); //--No password.  For assignment submission this would be expected to be empty.
        } catch (Exception e) {
            System.out.println("Could not make a connection to the database");
            return null;
        }

        try {

            Statement statement = conn.createStatement();

            //***************************************************
            // Select using statement
            //***************************************************
            //Next selectAll all the rows and display them here...
            ResultSet rs = statement.executeQuery("select * from ParkingPass");

            //Show all the students from the database
            while (rs.next()) {
                int id = rs.getInt("id");
                String firstName = rs.getString("firstName");
                String lastName = rs.getString("lastName");
                String licensePlate = rs.getString("licensePlate");
                String program = rs.getString("program");

                Parkingpass parkingPass = new Parkingpass(id, firstName, lastName, licensePlate, program);
                parkingPasses.add(parkingPass);
            }

        } catch (SQLException ex) {
            Logger.getLogger(ParkingPassDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                conn.close();
            } catch (SQLException ex) {
                //Could not close.  
                System.out.println("Error closing the connection.");
            }
        }
        return parkingPasses;

    }

    /**
     * Insert into the database.
     *
     * @param parkingPass Parking pass object to be inserted
     * @since 20201116
     * @author K.Taylor
     */
    public Parkingpass insert(Parkingpass parkingPass) {

        Connection conn = null;

        try {

            conn = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/cis2232_parking",
                    "root",
                    "");
        } catch (SQLException ex) {
            System.out.println("SQL exception happned!");

        }

        if (parkingPass.getId() == 0) {

            //***************************************************
            // INSERT
            //***************************************************
            try {
                String theStatement = "INSERT INTO ParkingPass(firstName,lastName,licensePlate, program) "
                        + "VALUES (?,?,?,?)";
                PreparedStatement stmt = conn.prepareStatement(theStatement);
                stmt.setString(1, parkingPass.getFirstName());
                stmt.setString(2, parkingPass.getLastName());
                stmt.setString(3, parkingPass.getLicensePlate());
                stmt.setString(4, parkingPass.getProgram());
                stmt.executeUpdate();

            } catch (SQLException sqle) {
                System.out.println("sql exception caught");
                sqle.printStackTrace();

            }
        } else {
            update(parkingPass);

        }

        return parkingPass;
    }

    /**
     * Update student information into the database.
     *
     * @param parkingPass Parking pass object to be updated
     * @return true if no exception occurs
     * @since 20201116
     * @author K.Taylor
     */
    public boolean update(Parkingpass parkingPass) {

        Connection conn = null;

        try {

            conn = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/cis2232_parking",
                    "root",
                    "");
        } catch (SQLException ex) {
            System.out.println("SQL exception happned!");
            return false;
        }

        if (parkingPass.getId() == null) {
            return false;
        } else {

            //***************************************************
            // update
            //***************************************************
            try {
                String theStatement = "UPDATE ParkingPass set firstName=?, lastName=?, "
                        + "licensePlate=?, program=? where id=?";
                PreparedStatement stmt = conn.prepareStatement(theStatement);
                stmt.setString(1, parkingPass.getFirstName());
                stmt.setString(2, parkingPass.getLastName());
                stmt.setString(3, parkingPass.getLicensePlate());
                stmt.setString(4, parkingPass.getProgram());
                stmt.setInt(5, parkingPass.getId());
                stmt.executeUpdate();
                return true;
            } catch (SQLException sqle) {
                System.out.println("sql exception caught");
                sqle.printStackTrace();
                return false;
            }
        }
    }

    /**
     * Delete the student from the database.
     *
     * @since 20201116
     * @author BJM
     *
     */
    public boolean delete(int id) {

        Connection conn = null;

        //***************************************************
        // INSERT
        //***************************************************
        try {
            conn = DatabaseUtility.getConnection("");
            String theStatement = "DELETE FROM ParkingPass WHERE id = ?";
            PreparedStatement stmt = conn.prepareStatement(theStatement);
            stmt.setInt(1, id);

            stmt.executeUpdate();

        } catch (SQLException sqle) {
            System.out.println("sql exception caught");
            sqle.printStackTrace();
            return false;
        }

        return true;
    }

}
