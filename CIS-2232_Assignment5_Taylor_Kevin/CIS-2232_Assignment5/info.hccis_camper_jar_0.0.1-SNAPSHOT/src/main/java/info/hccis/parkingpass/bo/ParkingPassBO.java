/**
 * @author K.Taylor
 * Date:11/16/2020
 * Subject: CIS-2232 Assignment 5 - Web Services
 * Instructor: BJ MacLean
 *
 *
 * Parking Pass Business Object class
 */
package info.hccis.parkingpass.bo;

import info.hccis.parkingpass.dao.ParkingPassDAO;
import info.hccis.parkingpass.jpa.entity.Parkingpass;
import info.hccis.parkingpass.repositories.ParkingPassRepository;
import java.util.ArrayList;

public class ParkingPassBO {

    public static ArrayList<Parkingpass> getParkingPasses(ParkingPassRepository ppr) {
        ArrayList<Parkingpass> parkingPasses = (ArrayList<Parkingpass>) ppr.findAll();

        return parkingPasses;
    }

    /**
     * Select all records from the database
     *
     * @return List of the parking passes
     * @since 20201116
     * @author K.Taylor
     */
    public ArrayList<Parkingpass> selectAll() {

        //Read from the database
        ParkingPassDAO parkingPassDAO = new ParkingPassDAO();
        ArrayList<Parkingpass> parkingPasses = parkingPassDAO.selectAll();
        return parkingPasses;
    }

    /**
     * Update the parking pass into the database
     *
     * @since 20201116
     * @author K.Taylor
     */
    public boolean update(Parkingpass parkingPass) {
        ParkingPassDAO parkingPassDAO = new ParkingPassDAO();
        return parkingPassDAO.update(parkingPass);
    }

    public Parkingpass save(Parkingpass parkingPass) {
        ParkingPassDAO parkingPassDAO = new ParkingPassDAO();

        if (parkingPass.getId() == 0) {
            parkingPass = parkingPassDAO.insert(parkingPass);
        } else {
            parkingPassDAO.update(parkingPass);
        }

        return parkingPass;
    }

    /**
     * Delete the student record
     *
     * @since 20201116
     * @author K.Taylor
     */
    public boolean delete(int id) {
        ParkingPassDAO parkingPassDAO = new ParkingPassDAO();
        return parkingPassDAO.delete(id);
    }

}
