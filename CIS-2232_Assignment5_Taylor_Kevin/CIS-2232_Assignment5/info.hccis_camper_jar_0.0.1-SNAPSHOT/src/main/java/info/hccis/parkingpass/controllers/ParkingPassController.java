/**
 * @author K.Taylor
 * Date:11/16/2020
 * Subject: CIS-2232 Assignment 5 - Web Services
 * Instructor: BJ MacLean
 *
 *
 * Parking Pass Controller class
 */
package info.hccis.parkingpass.controllers;

import info.hccis.parkingpass.bo.ParkingPassBO;
import info.hccis.parkingpass.jpa.entity.Parkingpass;
import info.hccis.parkingpass.repositories.ParkingPassRepository;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Controller for the camper functionality of the site
 *
 * @since 20201116
 * @author K.Taylor
 */
@Controller
@RequestMapping("/parkingPass")
public class ParkingPassController {

    private final ParkingPassRepository parkingPassRepository;

    @Autowired
    public ParkingPassController(ParkingPassRepository ppr) {
        parkingPassRepository = ppr;

    }

    /**
     * Page to allow user to view students
     *
     * @since 20201019
     * @author K.Taylor
     */
    @RequestMapping("/list")
    public String list(Model model) {

       
        model.addAttribute("parkingpasses", ParkingPassBO.getParkingPasses(parkingPassRepository));
        model.addAttribute("findNameMessage", "Students loaded");

        return "parkingPass/list";
    }

    /**
     * Page to allow user to add a students parking passes
     *
     * @since 20201116
     * @author K.Taylor
     */
    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public String add(Model model) {

        model.addAttribute("message", "Add new student");

        Parkingpass parkingPass = new Parkingpass();
        model.addAttribute("parkingPass", parkingPass);

        return "parkingPass/add";
    }

    /**
     * Page to allow user to submit the add a student. It will put the student
     * record in the database.
     *
     * @since 2020116
     * @author K.Taylor
     */
    @RequestMapping("/addSubmit")
    public String addSubmit(Model model, @Valid @ModelAttribute("parkingPass") Parkingpass parkingPass, BindingResult result) {

        //If errors put the object back in model and send back to the add page.
        if (result.hasErrors()) {
            System.out.println("Validation error");
            return "parkingPass/add";
        }

        
        parkingPassRepository.save(parkingPass);

        //reload the list
        
        String propFileName = "messages";
        ResourceBundle rb = ResourceBundle.getBundle(propFileName);
        String successAddString = rb.getString("message.parkingpass.saved");

        model.addAttribute("message", successAddString);
        model.addAttribute("parkingPasses", ParkingPassBO.getParkingPasses(parkingPassRepository));

        return "parkingPass/list";

    }

    /**
     * Page to allow user to edit a student record
     *
     * @since 20201116
     * @author K.Taylor
     */
    @RequestMapping("/edit")
    public String edit(Model model, HttpServletRequest request) {

        String idString = (String) request.getParameter("id");
        int id = Integer.parseInt(idString);
        System.out.println("BJTEST - id=" + id);

        //reload the list
        ParkingPassBO parkingPassBO = new ParkingPassBO();
        ArrayList<Parkingpass> parkingPasses = parkingPassBO.selectAll();

        Parkingpass found = null;
        for (Parkingpass current : parkingPasses) {
            if (current.getId() == id) {
                found = current;
                break;
            }
        }

        model.addAttribute("parkingPass", found);
        return "parkingPass/add";
    }

    /**
     * Page to allow user to delete a student record
     *
     * @since 20201019
     * @author K.Taylor
     */
    @RequestMapping("/delete")
    public String delete(Model model, HttpServletRequest request) {

        String idString = (String) request.getParameter("id");
        int id = Integer.parseInt(idString);
        System.out.println("id= " + id);

        //reload the list
//        ParkingPassBO parkingPassBO = new ParkingPassBO();
//        boolean deleted = parkingPassBO.delete(id);
        String propFileName = "messages";
        ResourceBundle rb = ResourceBundle.getBundle(propFileName);

        String successString;
        try {
            parkingPassRepository.deleteById(id);
            successString = rb.getString("message.parkingpass.deleted") + " (" + id + ")";
        } catch (EmptyResultDataAccessException e) {
            successString = rb.getString("message.parkingpass.deleted.error") + " (" + id + ")";
        }


        model.addAttribute("message", successString);
        model.addAttribute("parkingPasses", ParkingPassBO.getParkingPasses(parkingPassRepository));

        return "parkingPass/list";
    }

}
