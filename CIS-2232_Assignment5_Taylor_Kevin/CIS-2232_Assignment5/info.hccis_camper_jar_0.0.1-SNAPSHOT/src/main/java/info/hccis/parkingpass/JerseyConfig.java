/**
 * @author K.Taylor
 * Date:11/16/2020
 * Subject: CIS-2232 Assignment 5 - Web Services
 * Instructor: BJ MacLean
 *
 *
 * Parking Pass Jersey Config class
 */


package info.hccis.parkingpass;

import info.hccis.parkingpass.rest.ParkingPassService;
import javax.annotation.PostConstruct;
import javax.ws.rs.ApplicationPath;
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.stereotype.Component;

@Component
@ApplicationPath("api")
public class JerseyConfig extends ResourceConfig {

    @PostConstruct
    private void init() {
        registerClasses(ParkingPassService.class);
    }
}
