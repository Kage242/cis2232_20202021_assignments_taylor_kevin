/**
 * @author K.Taylor
 * Date:11/16/2020
 * Subject: CIS-2232 Assignment 5 - Web Services
 * Instructor: BJ MacLean
 *
 *
 * Parking Pass REST Service class
 */
package info.hccis.parkingpass.rest;

import com.google.gson.Gson;
import info.hccis.parkingpass.jpa.entity.Parkingpass;
import info.hccis.parkingpass.repositories.ParkingPassRepository;
import java.util.ArrayList;
import javax.ws.rs.Consumes;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.springframework.beans.factory.annotation.Autowired;

@Path("/ParkingPassService/parkingpasses")
public class ParkingPassService {

    private final ParkingPassRepository ppr;

    @Autowired
    public ParkingPassService(ParkingPassRepository ppr) {
        this.ppr = ppr;
    }

    /**
     * Converts the parking passes data to JSON file
     *
     * @return
     * @author: K.Taylor
     * @since 20201116
     */
    @GET
    @Produces("application/json")
    public ArrayList<Parkingpass> getAll() {
        ArrayList<Parkingpass> parkingPasses = (ArrayList<Parkingpass>) ppr.findAll();
        return parkingPasses;
    }

//    @GET
//    @Path("/{id}")
//    @Produces("application/json")
//    public Response getCamperById(@PathParam("id") int id) throws URISyntaxException {
//
//        Optional<Parkingpass> camper = cr.findById(id);
//
//        if (!camper.isPresent()) {
//            return Response.status(204).build();
//        } else {
//            return Response
//                    .status(200)
//                    .entity(camper).build();
//        }
//    }
    /**
     * Adds a new parking pass to the JSON file
     *
     * @param camperJson
     * @return
     * @author: K.Taylor
     * @since 20201116
     *
     */
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createCamper(String camperJson) {

        Gson gson = new Gson();
        Parkingpass parkingPass = gson.fromJson(camperJson, Parkingpass.class);

        if (parkingPass.getFirstName() == null || parkingPass.getFirstName().isEmpty()) {
            return Response.status(400).entity("Please provide all mandatory inputs").build();
        }

        if (parkingPass.getId() == null) {
            parkingPass.setId(0);
        }

        parkingPass = ppr.save(parkingPass);

        String temp = "";
        temp = gson.toJson(parkingPass);

        return Response.status(201).entity(temp).header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").build();
    }
//    
//    @DELETE
//    @Path("/{id}")
//    public Response deleteBooking(@PathParam("id") int id) throws URISyntaxException {
//        Optional<Booking> booking = br.findById(id);
//        if(booking != null) {
//            br.deleteById(id);
//            return Response.status(HttpURLConnection.HTTP_CREATED).build();
//        }
//        return Response.status(404).build();
//    }
//
//    @PUT
//    @Path("/{id}")
//    @Consumes("application/json")
//    @Produces("application/json")
//    public Response updateBooking(@PathParam("id") int id, String bookingJson) throws URISyntaxException 
//    {
//
//        Gson gson = new Gson();
//        Booking booking = gson.fromJson(bookingJson, Booking.class);
//        
//        if(booking.getName1() == null || booking.getName1().isEmpty()) {
//            return Response.status(400).entity("Please provide all mandatory inputs").build();
//        }
// 
//        if(booking.getId() == null){
//            booking.setId(0);
//        }
//
//        booking = br.save(booking);
////        BookingDAO bookingDAO = new BookingDAO();
////        bookingDAO.insert(booking);
//
//        String temp = "";
//        temp = gson.toJson(booking);
//
//        return Response.status(201).entity(temp).header("Access-Control-Allow-Origin", "*")
//                .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").build();        
//    }    

}
