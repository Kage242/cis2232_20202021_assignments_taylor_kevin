/**
 * @author K.Taylor
 * Date:11/16/2020
 * Subject: CIS-2232 Assignment 5 - Web Services
 * Instructor: BJ MacLean
 *
 *
 * Parking Pass Repository 
 */




package info.hccis.parkingpass.repositories;

import info.hccis.parkingpass.jpa.entity.Parkingpass;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ParkingPassRepository extends CrudRepository<Parkingpass, Integer> {
    
//    ArrayList<Parkingpass> findAllByDob(String dob);
//    ArrayList<Parkingpass> findAllByLastName(String lastName); //created but not used in project yet.
    
}