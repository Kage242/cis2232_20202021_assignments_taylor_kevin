/**
 * @author K.Taylor
 * Date:11/16/2020
 * Subject: CIS-2232 Assignment 5 - Web Services
 * Instructor: BJ MacLean
 *
 *
 * Parking Pass Calls REST Controller class
 */
package info.hccis.parkingpass.main;

import com.google.gson.Gson;
import info.hccis.parkingpass.util.UtilityRest;
import info.hccis.parkingpass.model.jpa.Parkingpass;

import static java.awt.SystemColor.info;
import java.util.Scanner;
import org.json.JSONArray;

public class Controller {

    final public static String MENU = "\nMain Menu \nA) Add\nU) Update\n"
            + "V) View\n"
            + "D) Delete\n"
            + "X) eXit";
    final static Scanner input = new Scanner(System.in);
    private static final String URL_STRING = "http://localhost:8081/api/ParkingPassService/parkingpasses";

    public static void main(String[] args) {
        boolean endProgram = false;
        do {
            System.out.println(MENU);
            String choice = input.nextLine();
            Parkingpass parkingPass;
            String url;

            switch (choice.toUpperCase()) {
//                case "A":
//                    parkingPass = createPass();
//                    url = URL_STRING;
//                    System.out.println("Url="+url);
//                    UtilityRest.addUsingRest(url, parkingPass);
//                    break;
//                case "U":
//                    booking = createBooking();
//                    url = URL_STRING+""+booking.getId();
//                    System.out.println("Url="+url);
//                    UtilityRest.updateUsingRest(url, booking);
//                    break;
//                case "D":
//                    System.out.println("Enter id to delete");
//                    Scanner input = new Scanner(System.in);
//                    int id = input.nextInt();
//                    input.nextLine();  //burn
//                    UtilityRest.deleteUsingRest(URL_STRING, id);
//                    break;

                /**
                 * Allows user to view all parking passes saved to the JSON file
                 */
                case "V":
                    String jsonReturned = UtilityRest.getJsonFromRest(URL_STRING);                  //**************************************************************
//                    //Based on the json string passed back, loop through each json
//                    //object which is a json string in an array of json strings.
//                    //*************************************************************
                    JSONArray jsonArray = new JSONArray(jsonReturned);
//                    //**************************************************************
//                    //For each json object in the array, show the first and last names
//                    //**************************************************************
                    System.out.println("Here are the rows");
                    System.out.println(jsonArray.toString());
                    break;

                case "X":
                    endProgram = true;
                    break;
                default:
                    System.out.println("INVALID OPTION");
            }
        } while (!endProgram);
    }

    /**
     * Create a parking pass object by passing asking user for input.
     *
     * @return camper
     * @since 20201116
     * @author K.Taylor
     *
     * public static Parkingpass createPass() { Parkingpass newPass = new
     * Parkingpass();
     *
     * System.out.println("Enter id: (0 for add)");
     * newPass.setId(Integer.parseInt(input.nextLine()));
     *
     * System.out.println("First name:");
     * newPass.setFirstName(input.nextLine());
     *
     * System.out.println("Last name:"); newPass.setLastName(input.nextLine());
     *
     * System.out.println("License Plate # :");
     * newPass.setLicensePlate(input.nextLine());
     *
     * System.out.println("Program:"); newPass.setProgram(input.nextLine());
     *
     * return newPass;
     *
     */
}
