/**
 * @author K.Taylor
 * Date:11/16/2020
 * Subject: CIS-2232 Assignment 5 - Web Services
 * Instructor: BJ MacLean
 *
 *
 * Parking Pass REST Entity class
 */

package info.hccis.parkingpass.model.jpa;

import java.io.Serializable;

/**
 *
 * @author Kevin
 */
public class Parkingpass implements Serializable {

    private static final long serialVersionUID = 1L;
    private Integer id;

    private String firstName;

    private String lastName;

    private String licensePlate;

    private String program;

    public Parkingpass() {
    }

    public Parkingpass(Integer id) {
        this.id = id;
    }

    public Parkingpass(Integer id, String firstName, String lastName, String licensePlate, String program) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.licensePlate = licensePlate;
        this.program = program;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getLicensePlate() {
        return licensePlate;
    }

    public void setLicensePlate(String licensePlate) {
        this.licensePlate = licensePlate;
    }

    public String getProgram() {
        return program;
    }

    public void setProgram(String program) {
        this.program = program;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Parkingpass)) {
            return false;
        }
        Parkingpass other = (Parkingpass) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "info.hccis.parkingpass.jpa.entity.Parkingpass[ id=" + id + " ]";
    }

}
