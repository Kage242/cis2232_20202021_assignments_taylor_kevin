/**
 * @author K.Taylor
 * Date:11/03/2020
 * Subject: CIS-2232 Flower Shop (Sprint 3)
 * Instructor: BJ MacLean
 * 
 * 
 * Flower Shop Summary Controller
 */
package info.hccis.flowershop.controllers;


import info.hccis.flowershop.bo.FlowerShopBO;
import info.hccis.flowershop.jpa.entity.FlowerShop;
import java.util.ArrayList;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
@RequestMapping("/summary")


public class SummaryController {
//    
// private final CustomerRepository customerRepository;
//
//    public SummaryController(CustomerRepository cr) {
//        customerRepository = cr;
//    }

  
//    private final CustomerRepository customerRepository;
//
//   public SummaryController(CustomerRepository cr) {
//       customerRepository = cr;
//    
//   }
    
    
    
    /**
     * Page to allow user to find a record by customer name
     *
     * @author K.Taylor
     */
    @RequestMapping("/ordersummary")
    public String getCustomerName(Model model) {

        FlowerShop flowerShop = new FlowerShop();
        model.addAttribute("flowerShop", flowerShop);

        return "summary/ordersummary";
    }

    /**
     * Page to allow user to submit the customer name
     *
     * @author K.Taylor
     */
    @RequestMapping("/submitOrderSummary")
    public String addSubmit(Model model, @ModelAttribute("flowerShop") FlowerShop flowerShop) {

        //Search database for record
        FlowerShopBO flowerShopBO = new FlowerShopBO();
       flowerShopBO.showCustomer();
       ArrayList<FlowerShop> flowerShopList = flowerShopBO.showCustomer();
       //return flowerShopList;

        
       model.addAttribute("flowerShopList", flowerShopList);
        return "flowerShop/list";

    }

    /**
     * Displays customer record in database from search
     * @param fullName
     * @return
     * 
     * @author K.Taylor
     */
   public ArrayList<FlowerShop> loadCustomerByName(String fullName) {
        FlowerShopBO flowerShopBO = new FlowerShopBO();
        ArrayList<FlowerShop> flowerShopList = flowerShopBO.showCustomer();
       
        return flowerShopList;
}
       
}
        
   
//     ArrayList<FlowerShop> flowerShopList = (ArrayList<FlowerShop>) customerRepository.findAllByName(fullName);
//      HashMap<Integer, String> customerTypesMap = CustomerTypeBO.getCustomerTypesMap();
//     for (FlowerShop current : flowerShopList) {
//          current.setCustomerTypeDescription(customerTypesMap.get(current.getCustomerTypeId()));
//      }
   




