/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.hccis.flowershop.bo;

import info.hccis.flowershop.dao.CustomerTypeDAO;
import info.hccis.flowershop.jpa.entity.CustomerType;
import info.hccis.flowershop.dao.CustomerTypeDAO;
import java.util.ArrayList;
import java.util.HashMap;


/**
 *
 * @author Kevin
 */
public class CustomerTypeBO {

/**
 * CamperType business object
 *
 * @author CIS2232
 * @since 20201016
 */

    private static HashMap<Integer, String> customerTypesMap = new HashMap();

    public static HashMap<Integer, String> getCustomerTypesMap() {
        return customerTypesMap;
    }

    public static void setCustomerTypesMap(HashMap<Integer, String> customerTypesMap) {
        CustomerTypeBO.customerTypesMap = customerTypesMap;
    }



    
    
    
    
    /**
     * Connect to the data access object to get the campers from the datasource.
     *
     * @since 20200923
     * @author BJM
     */
    public ArrayList<CustomerType> load() {

        //Read from the database
        CustomerTypeDAO customerTypeDAO = new CustomerTypeDAO();
        ArrayList<CustomerType> customerTypes = customerTypeDAO.select();

        
        
        return customerTypes;
    }
}
    











