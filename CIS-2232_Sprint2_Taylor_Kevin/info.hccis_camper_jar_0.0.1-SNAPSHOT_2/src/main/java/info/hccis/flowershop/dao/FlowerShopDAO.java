/**
 * @author K.Taylor
 * Date:11/03/2020
 * Subject: CIS-2232 Flower Shop (Sprint 3)
 * Instructor: BJ MacLean
 *
 * Flower Shop database access object
 */
package info.hccis.flowershop.dao;

import info.hccis.flowershop.jpa.entity.FlowerShop;
import info.hccis.flowershop.util.DatabaseUtility;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class FlowerShopDAO {

    public static boolean checkConnection() {

        Connection conn = null;
        try {
            conn = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/cis2232_flowers",
                    "root",
                    "");
        } catch (Exception e) {
            return false;
        }

        return true;

    }

    /**
     * Select the customers records from the database
     *
     */
    public ArrayList<FlowerShop> loadAll() {
        ArrayList<FlowerShop> flowerShopList = new ArrayList();

        //Select from the database
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/cis2232_flowers",
                    "root",
                    "");
        } catch (Exception e) {
            System.out.println("Could not make a connection to the database");
            return null;
        }

        try {

            Statement statement = conn.createStatement();

            //***************************************************
            // Select using statement
            //***************************************************
            //Next selectAll all the rows and display them here...
            ResultSet rs = statement.executeQuery("SELECT * FROM customer");

            //Show all the customers from the database
            while (rs.next()) {
                int id = rs.getInt("id");
                int customerTypeId = rs.getInt("customerTypeId");
                String customerName = rs.getString("fullName");
                String address = rs.getString("address1");
                String city = rs.getString("city");
                String province = rs.getString("province");
                String postalCode = rs.getString("postalCode");
                String phoneNumber = rs.getString("phoneNumber");
                String birthDate = rs.getString("birthDate");
                String loyaltyCard = rs.getString("loyaltyCard");

                FlowerShop flowerShop = new FlowerShop(id, customerTypeId, customerName, address, city, province, postalCode, phoneNumber, birthDate, loyaltyCard);
                flowerShopList.add(flowerShop);
            }

        } catch (SQLException ex) {
            Logger.getLogger(FlowerShopDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                conn.close();
            } catch (SQLException ex) {
                //Could not close.  
                System.out.println("Error closing the connection.");
            }
        }
        return flowerShopList;

    }

    /**
     * Insert into the database.
     *
     * @param flowerShop Flower shop object to be inserted
     * @author K.Taylor
     * @return
     *
     */
    public FlowerShop insert(FlowerShop flowerShop) {

        Connection conn = null;

        try {

            conn = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/cis2232_flowers",
                    "root",
                    "");
        } catch (SQLException ex) {
            System.out.println("SQL exception happned!");

        }

        if (flowerShop.getId() == 0) {

            //***************************************************
            // INSERT
            //***************************************************
            try {
                String theStatement = "INSERT INTO customer(customerTypeId, fullName,address1,city, province, postalCode, phoneNumber,birthDate,loyaltyCard) "
                        + "VALUES (?,?,?,?,?,?,?,?,?)";
                PreparedStatement stmt = conn.prepareStatement(theStatement);
                stmt.setInt(1, flowerShop.getCustomerTypeId());
                stmt.setString(2, flowerShop.getFullName());
                stmt.setString(3, flowerShop.getAddress());
                stmt.setString(4, flowerShop.getCity());
                stmt.setString(5, flowerShop.getProvince());
                stmt.setString(6, flowerShop.getPostalCode());
                stmt.setString(7, flowerShop.getPhoneNumber());
                stmt.setString(8, flowerShop.getBirthDate());
                stmt.setString(9, flowerShop.getLoyaltyCard());

                stmt.executeUpdate();

            } catch (SQLException sqle) {
                System.out.println("sql exception caught");
                sqle.printStackTrace();

            }
        } else {
            update(flowerShop);

        }

        return flowerShop;
    }

    /**
     * Update customer information into the database.
     *
     * @param flowerShop Flower shop object to be updated
     * @return true if no exception occurs
     * @author K.Taylor
     */
    public boolean update(FlowerShop flowerShop) {

        Connection conn = null;

        try {

            conn = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/cis2232_flowers",
                    "root",
                    "");
        } catch (SQLException ex) {
            System.out.println("SQL exception happned!");
            return false;
        }

        if (flowerShop.getId() == 0) {
            return false;
        } else {

            //***************************************************
            // update
            //***************************************************
            try {
                String theStatement = "UPDATE customer set customerTypeId=?, fullName=?,address1=?, city=?, province=?, postalCode =? , phoneNumber=?, birthDate=?, loyaltyCard =?"
                        + "where id=?";

                PreparedStatement stmt = conn.prepareStatement(theStatement);

                stmt.setInt(1, flowerShop.getCustomerTypeId());
                stmt.setString(2, flowerShop.getFullName());
                stmt.setString(3, flowerShop.getAddress());
                stmt.setString(4, flowerShop.getCity());
                stmt.setString(5, flowerShop.getProvince());
                stmt.setString(6, flowerShop.getPostalCode());
                stmt.setString(7, flowerShop.getPhoneNumber());
                stmt.setString(8, flowerShop.getBirthDate());
                stmt.setString(9, flowerShop.getLoyaltyCard());
                stmt.setInt(10, flowerShop.getId());

                stmt.executeUpdate();
                return true;
            } catch (SQLException sqle) {
                System.out.println("sql exception caught");
                sqle.printStackTrace();
                return false;
            }
        }
    }

    /**
     * Delete the student from the database.
     *
     * @since 20201009
     * @author BJM
     *
     */
    public boolean delete(int id) {

        Connection conn = null;

        //***************************************************
        // INSERT
        //***************************************************
        try {
            conn = DatabaseUtility.getConnection("");
            String theStatement = "DELETE FROM customer WHERE id = ?";
            PreparedStatement stmt = conn.prepareStatement(theStatement);
            stmt.setInt(1, id);

            stmt.executeUpdate();

        } catch (SQLException sqle) {
            System.out.println("sql exception caught");
            sqle.printStackTrace();
            return false;
        }

        return true;
    }

    public ArrayList<FlowerShop> search() {
        ArrayList<FlowerShop> flowerShopList = new ArrayList();

        Connection conn = null;
        try {

            //Select the campers from the database
            conn = DatabaseUtility.getConnection("");
            Statement statement = conn.createStatement();

            //***************************************************
            // Select using statement
            //***************************************************
            //Next select all the rows and display them here...
            ResultSet rs = statement.executeQuery("select * from customer, customertype, flowerorder, itemtype, orderstatustype WHERE customer.fullName = \"?\"");

            //Show all the campers
            while (rs.next()) {

                String fullName = rs.getString("fullName");
                String customerType = rs.getString("customertype");
                String flowerOrder = rs.getString("flowerorder");
                String itemType = rs.getString("itemtype");
                String orderStatus = rs.getString("orderstatustype");
                int id = rs.getInt("id");

                FlowerShop flowerShop2 = new FlowerShop();
                flowerShop2.setId(id);
                flowerShop2.setFullName(fullName);
                flowerShop2.setCustomerTypeDescription(customerType);
                flowerShop2.setFlowerOrder(flowerOrder);
                flowerShop2.setItemType(itemType);
                flowerShop2.setOrderStatus(orderStatus);

                flowerShopList.add(flowerShop2);
            }

        } catch (SQLException ex) {
            Logger.getLogger(CustomerTypeDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                conn.close();
            } catch (SQLException ex) {
                //Could not close.  
                System.out.println("Error closing the connection.");
            }
        }
        return flowerShopList;

    }

    /**
     * This method saves all the information from the customer table to a JSON
     * file
     *
     * @return 
     * author: K.Taylor
     */
    public ArrayList<FlowerShop> saveToJSON() {

        ArrayList<FlowerShop> flowerShopList = new ArrayList();
        //Creating a JSONObject object
        JSONObject jsonObject = new JSONObject();
        JSONArray jsonArray = new JSONArray();
        //Select from the database
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/cis2232_flowers",
                    "root",
                    "");
        } catch (Exception e) {
            System.out.println("Could not make a connection to the database");
            return null;
        }

        try {

            Statement statement = conn.createStatement();

            //***************************************************
            // Select using statement
            //***************************************************
            //Next selectAll all the rows and display them here...
            ResultSet rs = statement.executeQuery("SELECT * FROM customer");

            //Show all the customers from the database
            while (rs.next()) {
                int id = rs.getInt("id");
                int customerTypeId = rs.getInt("customerTypeId");
                String customerName = rs.getString("fullName");
                String address = rs.getString("address1");
                String city = rs.getString("city");
                String province = rs.getString("province");
                String postalCode = rs.getString("postalCode");
                String phoneNumber = rs.getString("phoneNumber");
                String birthDate = rs.getString("birthDate");
                String loyaltyCard = rs.getString("loyaltyCard");

                JSONObject custRecord = new JSONObject();
                //Inserting key-value pairs into the json object
                custRecord.put("id", id);
                custRecord.put("customerTypeId", customerTypeId);
                custRecord.put("fullName", customerName);
                custRecord.put("address1", address);
                custRecord.put("city", city);
                custRecord.put("province", province);
                custRecord.put("postalCode", postalCode);
                custRecord.put("phoneNumber", phoneNumber);
                custRecord.put("birthDate", birthDate);
                custRecord.put("loyaltyCard", loyaltyCard);

                //Save data to JSON file
                jsonArray.put(custRecord);
                jsonObject.put("Customer_Information", jsonArray);
                FileWriter fileWrite = new FileWriter("C:/flowers/customers_2020110990930.json");
                fileWrite.write(jsonObject.toString());
                fileWrite.close();
            }

        } catch (SQLException ex) {
            Logger.getLogger(FlowerShopDAO.class.getName()).log(Level.SEVERE, null, ex);
        } catch (JSONException ex) {
            Logger.getLogger(FlowerShopDAO.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(FlowerShopDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                conn.close();
            } catch (SQLException ex) {
                //Could not close.  
                System.out.println("Error closing the connection.");
            }

        }
        return flowerShopList;

    }

}
