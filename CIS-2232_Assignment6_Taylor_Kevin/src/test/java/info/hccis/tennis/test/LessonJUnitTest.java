package info.hccis.tennis.test;

import info.hccis.tennis.Lesson;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

/**
 *Since: 20201203
 * @author Kevin
 * 
 * 
 * 
 * This class is is used to run unit test from the Lesson class
 */
public class LessonJUnitTest {

    public LessonJUnitTest() {
    }

    @BeforeAll
    public static void setUpClass() {
    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {
    }

    @AfterEach
    public void tearDown() {
    }

    /**
     * Tests the private member in the Calculate Lessons 
     * Since: 20201203
     *
     * @author K.Taylor
     */
    @Test
    public void testTennisLessons_PrivateGroup_Member() {

        Lesson tennis = new Lesson();
        tennis.setNumGroup(1);
        tennis.setMemberDecision("Y");

        int results = tennis.CalculateLessons();
        Assertions.assertEquals(tennis.getPRIVATEGROUP_MEMBER(), results);

    }

    /**
     * Tests the private non-member in the Calculate Lessons 
     * Since: 20201203
     *
     * @author K.Taylor
     */
    @Test
    public void testTennisLessons_PrivateGroup_NonMember() {

        Lesson tennis = new Lesson();
        tennis.setNumGroup(1);
        tennis.setMemberDecision("N");

        int results = tennis.CalculateLessons();
        Assertions.assertEquals(tennis.getPRIVATEGROUP_NONMEMBER(), results);

    }

    /**
     * Tests the group of 2 members in the Calculate Lessons 
     * Since: 20201203
     *
     * @author K.Taylor
     */
    @Test
    public void testTennisLessons_Group2_Member() {

        Lesson tennis = new Lesson();
        tennis.setNumGroup(2);
        tennis.setMemberDecision("Y");

        int results = tennis.CalculateLessons();
        Assertions.assertEquals(tennis.getGROUP2_MEMBER(), results);

    }

    /**
     * Tests the group of 2 non-members in the Calculate 
     * Lessons Since: 20201203
     *
     * @author K.Taylor
     */
    @Test
    public void testTennisLessons_Group2_NonMember() {

        Lesson tennis = new Lesson();
        tennis.setNumGroup(2);
        tennis.setMemberDecision("N");

        int results = tennis.CalculateLessons();
        Assertions.assertEquals(tennis.getGROUP2_NONMEMBER(), results);

    }

    /**
     * Tests the group of 3 members in the Calculate Lessons 
     * Since: 20201203
     *
     * @author K.Taylor
     */
    @Test
    public void testTennisLessons_Group3_Member() {

        Lesson tennis = new Lesson();
        tennis.setNumGroup(3);
        tennis.setMemberDecision("Y");

        int results = tennis.CalculateLessons();
        Assertions.assertEquals(tennis.getGROUP3_MEMBER(), results);

    }

    /**
     * Tests the group of 3 non-members in the Calculate Lessons 
     * Since: 20201203
     *
     * @author K.Taylor
     */
    @Test
    public void testTennisLessons_Group3_NonMember() {

        Lesson tennis = new Lesson();
        tennis.setNumGroup(3);
        tennis.setMemberDecision("N");

        int results = tennis.CalculateLessons();
        Assertions.assertEquals(tennis.getGROUP3_NONMEMBER(), results);

    }

    /**
     * Tests the group of 4 members in the Calculate Lessons 
     * Since: 20201203
     *
     * @author K.Taylor
     */
    @Test
    public void testTennisLessons_Group4_Member() {

        Lesson tennis = new Lesson();
        tennis.setNumGroup(4);
        tennis.setMemberDecision("Y");

        int results = tennis.CalculateLessons();
        Assertions.assertEquals(tennis.getGROUP4_MEMBER(), results);

    }

    /**
     * Tests the group of 4 non-members in the Calculate Lessons 
     * Since: 20201203
     *
     * @author K.Taylor
     */
    @Test
    public void testTennisLessons_Group4_NonMember() {

        Lesson tennis = new Lesson();
        tennis.setNumGroup(4);
        tennis.setMemberDecision("N");

        int results = tennis.CalculateLessons();
        Assertions.assertEquals(tennis.getGROUP4_NONMEMBER(), results);

    }

}
