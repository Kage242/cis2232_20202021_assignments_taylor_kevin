package info.hccis.tennis.controller;

import info.hccis.tennis.Lesson;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;
import javax.swing.JOptionPane;

/**
 * Since: 20201203
 *
 * @author K.Taylor
 *
 *S
 *
 * This class is the controller of the application and runs the threads
 */
public class Runner extends Thread {
    //Creates file to save data
    File output = new File("lesson.txt");

    //Variables used
    String endThread = "Exit";
    Scanner console = new Scanner(System.in);
    JOptionPane gui = new JOptionPane();

    Lesson tennis = new Lesson();
    
/**
 * This method allows the user to run the application until they exit
 * 
 */
    public void run() {
        do {
            System.out.println("Welcome to the CIS Tennis Program");
            gui.showMessageDialog(null, "Welcome to the CIS Tennis Program");

            System.out.println("How many are in the group (1,2,3,4)");
            String guiNumGroup = gui.showInputDialog(null, "How many are in the group (1,2,3,4)");
            tennis.setNumGroup(console.nextInt());
            tennis.setNumGroup(Integer.parseInt(guiNumGroup));

            System.out.println("Are you a member (Y/N)?");
            String guiMember = gui.showInputDialog(null, "Are you a member (Y/N)?");
            tennis.setMemberDecision(console.next());
            tennis.setMemberDecision(guiMember);

            System.out.println("How many hours do you want for your lesson?");
            String guiNumHours = gui.showInputDialog(null, "How many hours do you want for your lesson?");
            tennis.setNumHours(console.nextInt());
            tennis.setNumHours(Integer.parseInt(guiNumHours));

            tennis.CalculateLessons();
            System.out.println(tennis.toString());
            gui.showMessageDialog(null, tennis.toString());
            
            //Saves data to text file
            try {
                FileWriter writer = new FileWriter(output);
                writer.write(tennis.toString());
                writer.flush();
                writer.close();
            } catch (IOException e) {
                System.out.println("There was an error trying to save the file!");

            }
        } while (console.next() == endThread);

        try {
            Thread.sleep(1000);
        } catch (InterruptedException ex) {
            System.out.println("Interrupted");
        }
    }
   
    
/**
 * 
 * This method runs the application
 * @param args 
 * Since: 20201203
 * @author: K.Taylor
 */
    public static void main(String[] args) {
        Runner thread1 = new Runner();

        thread1.start();

    }

}
