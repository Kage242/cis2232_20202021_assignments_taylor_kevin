package info.hccis.tennis;

import java.util.Scanner;

/**
 * Since:20201203
 * @author K.Taylor
 * 
 * 
 * This class stores the getters and setters used to update/access variables
 * Also, it stores a method that is used to calculate lessons and for unit testing 
 */
public class Lesson {

    
    //Getters/Setters for all varialbes used
    
    /**
     * @return the total
     */
    public int getTotal() {
        return total;
    }

    /**
     * @param total the total to set
     */
    public void setTotal(int total) {
        this.total = total;
    }

    /**
     * @return the PRIVATEGROUP_MEMBER
     */
    public int getPRIVATEGROUP_MEMBER() {
        return PRIVATEGROUP_MEMBER;
    }

    /**
     * @param PRIVATEGROUP_MEMBER the PRIVATEGROUP_MEMBER to set
     */
    public void setPRIVATEGROUP_MEMBER(int PRIVATEGROUP_MEMBER) {
        this.PRIVATEGROUP_MEMBER = PRIVATEGROUP_MEMBER;
    }

    /**
     * @return the PRIVATEGROUP_NONMEMBER
     */
    public int getPRIVATEGROUP_NONMEMBER() {
        return PRIVATEGROUP_NONMEMBER;
    }

    /**
     * @param PRIVATEGROUP_NONMEMBER the PRIVATEGROUP_NONMEMBER to set
     */
    public void setPRIVATEGROUP_NONMEMBER(int PRIVATEGROUP_NONMEMBER) {
        this.PRIVATEGROUP_NONMEMBER = PRIVATEGROUP_NONMEMBER;
    }

    /**
     * @return the GROUP2_MEMBER
     */
    public int getGROUP2_MEMBER() {
        return GROUP2_MEMBER;
    }

    /**
     * @param GROUP2_MEMBER the GROUP2_MEMBER to set
     */
    public void setGROUP2_MEMBER(int GROUP2_MEMBER) {
        this.GROUP2_MEMBER = GROUP2_MEMBER;
    }

    /**
     * @return the GROUP2_NONMEMBER
     */
    public int getGROUP2_NONMEMBER() {
        return GROUP2_NONMEMBER;
    }

    /**
     * @param GROUP2_NONMEMBER the GROUP2_NONMEMBER to set
     */
    public void setGROUP2_NONMEMBER(int GROUP2_NONMEMBER) {
        this.GROUP2_NONMEMBER = GROUP2_NONMEMBER;
    }

    /**
     * @return the GROUP3_MEMBER
     */
    public int getGROUP3_MEMBER() {
        return GROUP3_MEMBER;
    }

    /**
     * @param GROUP3_MEMBER the GROUP3_MEMBER to set
     */
    public void setGROUP3_MEMBER(int GROUP3_MEMBER) {
        this.GROUP3_MEMBER = GROUP3_MEMBER;
    }

    /**
     * @return the GROUP3_NONMEMBER
     */
    public int getGROUP3_NONMEMBER() {
        return GROUP3_NONMEMBER;
    }

    /**
     * @param GROUP3_NONMEMBER the GROUP3_NONMEMBER to set
     */
    public void setGROUP3_NONMEMBER(int GROUP3_NONMEMBER) {
        this.GROUP3_NONMEMBER = GROUP3_NONMEMBER;
    }

    /**
     * @return the GROUP4_MEMBER
     */
    public int getGROUP4_MEMBER() {
        return GROUP4_MEMBER;
    }

    /**
     * @param GROUP4_MEMBER the GROUP4_MEMBER to set
     */
    public void setGROUP4_MEMBER(int GROUP4_MEMBER) {
        this.GROUP4_MEMBER = GROUP4_MEMBER;
    }

    /**
     * @return the GROUP4_NONMEMBER
     */
    public int getGROUP4_NONMEMBER() {
        return GROUP4_NONMEMBER;
    }

    /**
     * @param GROUP4_NONMEMBER the GROUP4_NONMEMBER to set
     */
    public void setGROUP4_NONMEMBER(int GROUP4_NONMEMBER) {
        this.GROUP4_NONMEMBER = GROUP4_NONMEMBER;
    }

    /**
     * @return the numGroup
     */
    public int getNumGroup() {
        return numGroup;
    }

    /**
     * @param numGroup the numGroup to set
     */
    public void setNumGroup(int numGroup) {
        this.numGroup = numGroup;
    }

    /**
     * @return the numHours
     */
    public int getNumHours() {
        return numHours;
    }

    /**
     * @param numHours the numHours to set
     */
    public void setNumHours(int numHours) {
        this.numHours = numHours;
    }

    /**
     * @return the memberDecison
     */
    public String getMemberDecision() {
        return memberDecision;
    }

    /**
     * @param memberDecision the memberDecison to set
     */
    public void setMemberDecision(String memberDecision) {
        this.memberDecision = memberDecision;
    }
     //Variables used for calcalutions
    private int numGroup;
    private int numHours;
    private String memberDecision;
    private int PRIVATEGROUP_MEMBER = 55;
    private int PRIVATEGROUP_NONMEMBER = 60;
    private int GROUP2_MEMBER = 30;
    private int GROUP2_NONMEMBER = 33;
    private int GROUP3_MEMBER = 21;
    private int GROUP3_NONMEMBER = 23;
    private int GROUP4_MEMBER = 16;
    private int GROUP4_NONMEMBER = 18;
    private int total;

    //Capture user input
    Scanner input = new Scanner(System.in);

    /**
     * 
     * This method calculates the tennis lesson based on the user input
     * Also, this method is used for unit testing in the LessonUnitTest class
     */
    public int CalculateLessons() {
      
        if ((numGroup == 1) && (memberDecision == "Y")) {
            setTotal(55);

        } else if ((numGroup == 1) && (memberDecision == "N")) {
            setTotal(60);
        } else if ((numGroup == 2) && (memberDecision == "Y")) {
            setTotal(30);
        } else if ((numGroup == 2) && (memberDecision == "N")) {
            setTotal(33);

        } else if ((numGroup == 3) && (memberDecision == "Y")) {
            setTotal(21);

        } else if ((numGroup == 3) && (memberDecision == "N")) {
            setTotal(23);

        } else if ((numGroup == 4) && (memberDecision == "Y")) {
            setTotal(16);
        } else if ((numGroup == 4) && (memberDecision == "N")) {
            setTotal(18);
        }

        return getTotal();
    }

   
    /**
     * 
     * This method is used to display the details of the lesson from the user
     * Since: 20201203
     * @author: K.Taylor
     * @return 
     */
    public String toString() {

        return "Lesson Details - Lesson group size: " + getNumGroup() + " Member: " + getMemberDecision()
                + " Rate: $" + getNumHours() + "/hour Cost: $" + getTotal();

    }
}
