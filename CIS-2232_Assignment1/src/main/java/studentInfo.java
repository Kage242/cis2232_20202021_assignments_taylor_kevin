
import java.io.*;
import java.util.Scanner;

/**
 * Author: Kevin Taylor Date: 09/21/2020 Subject: CIS-2232 Assignment 1
 * Instructor: BJ MacLean
 *
 * This class stores variables and methods used within the application
 */
public class studentInfo {

    /**
     * Variables used
     */
    String studentName;
    int studentID;
    String reflectionTxt;
    String reflectionTxt2;
    String userChoice;
    String FILE_LOCATION
            = "cis2232" + File.separator + "reflection.json";
    Scanner input = new Scanner(System.in);

    /**
     * This method creates a file and adds the new student to the file.
     */
    public void addStudent() {
        /**
         * This creates the file and allows the user to add as much info as
         * needed
         */
        try {
            BufferedWriter bufferWrite = new BufferedWriter(new FileWriter("reflections.txt", true));
            PrintWriter printWrite = new PrintWriter(bufferWrite);

            /**
             * This prompts the user for their info
             */
            System.out.println("What is your name");
            studentName = input.nextLine();
            System.out.println("What is your reflection text?");
            reflectionTxt = input.nextLine();
            System.out.println("Choice:  (U-Update reflection, Enter to skip)");
            userChoice = input.nextLine();
            System.out.println("What is your student number?");
            studentID = input.nextInt();

            if (userChoice == "U") {
                System.out.println("Additional reflection: ");
                reflectionTxt2 = input.nextLine();
            } else {
                reflectionTxt2 = "";
            }
            /**
             * This displays a message to let the user know their info has been
             * created and saved
             */
            System.out.println("New Student Created! \n" + "About to save: " + "\n" + studentName + "\n" + studentID + "\n" + reflectionTxt + "\n" + reflectionTxt2);
            printWrite.println(studentName);
            printWrite.println(studentID);
            printWrite.println(reflectionTxt);
            printWrite.println(reflectionTxt2);
            printWrite.close();

        } catch (IOException e) {
            System.out.println("Error! File doesn't exist");
        }

    }

    /**
     * This method displays all the students that have been added to the file
     */
    public void showStudents() {
        try {
            FileInputStream fileStream = new FileInputStream("reflections.txt");
            DataInputStream dataStream = new DataInputStream(fileStream);

            BufferedReader br = new BufferedReader(new InputStreamReader(dataStream));

            String results;
            while ((results = br.readLine()) != null) {
                System.out.println(results + "\n");

            }
            dataStream.close();

        } catch (IOException e2) {
            System.out.println("Error! Failed to load file");
        }

    }
}
