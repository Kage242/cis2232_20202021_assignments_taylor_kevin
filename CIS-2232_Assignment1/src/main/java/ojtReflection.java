
import javax.swing.JOptionPane;

/**
 * Author: Kevin Taylor
 * Date:09/21/2020
 * Subject: CIS-2232 Assignment 1
 * Instructor: BJ MacLean
 * <p>
 * This class processes the Menu option based on user input and executes the application.
 */

public class ojtReflection {
    /**
     * This method creates the Menu for this application
     */
    public void StartApp() {
        final String MENU
                = "-------------------------\n"
                + "- A- Add new Student\n"
                + "- S- Show Student Info\n"
                + "- X- Quit Game\n"
                + "-------------------------\n";

        final String endApp = "X";
        String option = "";

        /**
         *
         * This loop allows the menu to run as long as the user doesn't
         * quit
         */
        do {
            option = JOptionPane.showInputDialog(MENU);
            processMenuOption(option);
        } while (!option.equalsIgnoreCase(endApp));
        JOptionPane.showInputDialog(null, MENU);
    }

    /**
     * This method is used to process the user input based on their choice
     *
     * @param option
     */
    public static void processMenuOption(String option) {
        studentInfo si = new studentInfo();
        /**
         * This switch does an action based on the user input
         */
        switch (option.toUpperCase()) {
            case "A":
                si.addStudent();
                break;

            case "S":
                si.showStudents();
                break;
            case "X":
                JOptionPane.showMessageDialog(null, "Goodbye! Please press X again to exit.");
                break;
            default:
                JOptionPane.showMessageDialog(null, "Invalid entry");
        }
    }

    /**
     * This method executes the application - OJT Reflections
     *
     * @param args
     */
    public static void main(String[] args) {
        ojtReflection ojtReflect = new ojtReflection();
        ojtReflect.StartApp();
    }

}