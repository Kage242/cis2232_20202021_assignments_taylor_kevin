DROP DATABASE if exists cis2232_camper;
CREATE DATABASE cis2232_camper;
USE cis2232_camper;

CREATE TABLE Camper (
  id int(5) NOT NULL,
  firstName varchar(100) NOT NULL COMMENT 'First Name',
  lastName varchar(100) NOT NULL COMMENT 'Last Name',
  dob varchar(10) DEFAULT NULL COMMENT 'yyyy-MM-dd',
  camperType int(5) DEFAULT NULL COMMENT 'reference to CamperType.id',
  createdDateTime varchar(20)  DEFAULT current_timestamp() COMMENT 'When record was created. yyyy-MM-dd hh:mm'
);


ALTER TABLE Camper
  ADD PRIMARY KEY (id);

ALTER TABLE Camper
  MODIFY id int(5) NOT NULL AUTO_INCREMENT;

INSERT INTO Camper (firstName, lastName, dob) VALUES
('Michael', 'Sawyer', '1998-08-15'),
('Parker', 'Gallant', '2000-02-22'),
('Khari', 'Woods', '2002-12-25');

CREATE TABLE CamperType(
  id int(5) NOT NULL,
  description varchar(100) DEFAULT NULL COMMENT 'Camper Type Description',
  dailyCost double
);

ALTER TABLE CamperType
  ADD PRIMARY KEY (id);

ALTER TABLE CamperType
  MODIFY id int(5) NOT NULL AUTO_INCREMENT;

INSERT INTO CamperType (id, description, dailyCost) VALUES ('0', 'Beginner', '15')
, ('0', 'Intermediate', '17.5')
, ('0', 'Senior', '20');


alter table Camper add foodCost int after camperType;

