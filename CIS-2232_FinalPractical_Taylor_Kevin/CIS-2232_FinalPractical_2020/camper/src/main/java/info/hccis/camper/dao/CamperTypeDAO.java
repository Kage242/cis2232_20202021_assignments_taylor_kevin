package info.hccis.camper.dao;

import info.hccis.camper.jpa.entity.Camper;
import info.hccis.camper.jpa.entity.CamperType;
import info.hccis.camper.util.DatabaseUtility;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Camper Type database access object
 *
 * @author bjm
 * @since 20201016
 */
public class CamperTypeDAO {

    /**
     * Select the records from the database
     *
     * @since 20201016
     * @author BJM
     */
    public ArrayList<CamperType> select() {
        ArrayList<CamperType> camperTypes = new ArrayList();

        Connection conn = null;
        try {

            //Select the campers from the database
            conn = DatabaseUtility.getConnection("");
            Statement statement = conn.createStatement();

            //***************************************************
            // Select using statement
            //***************************************************
            //Next select all the rows and display them here...
            ResultSet rs = statement.executeQuery("select * from CamperType");

            //Show all the campers
            while (rs.next()) {
                int id = rs.getInt("id");
                String description= rs.getString("description");
                Double dailyCost = rs.getDouble("dailyCost");

                CamperType camperType = new CamperType();
                camperType.setId(id);
                camperType.setDescription(description);
                camperTypes.add(camperType);
            }

        } catch (SQLException ex) {
            Logger.getLogger(CamperTypeDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                conn.close();
            } catch (SQLException ex) {
                //Could not close.  
                System.out.println("Error closing the connection.");
            }
        }
        return camperTypes;

    }
}
