package info.hccis.camper.bo;

import info.hccis.camper.dao.CamperDAO;
import info.hccis.camper.jpa.entity.Camper;
import info.hccis.camper.jpa.entity.CamperType;
import info.hccis.camper.repositories.CamperRepository;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Camper business object with methods for testing and new attributes
 *
 * @author K.Taylor
 * @since 20201210
 */
public class CamperBO {

    /**
     * @return the camperType
     */
    public int getCamperType() {
        return camperType;
    }

    /**
     * @param camperType the camperType to set
     */
    public void setCamperType(int camperType) {
        this.camperType = camperType;
    }

    /**
     * @return the CAMPER_TYPE_1
     */
    public int getCAMPER_TYPE_1() {
        return CAMPER_TYPE_1;
    }

    /**
     * @param CAMPER_TYPE_1 the CAMPER_TYPE_1 to set
     */
    public void setCAMPER_TYPE_1(int CAMPER_TYPE_1) {
        this.CAMPER_TYPE_1 = CAMPER_TYPE_1;
    }

    /**
     * @return the CAMPER_TYPE_2
     */
    public int getCAMPER_TYPE_2() {
        return CAMPER_TYPE_2;
    }

    /**
     * @param CAMPER_TYPE_2 the CAMPER_TYPE_2 to set
     */
    public void setCAMPER_TYPE_2(int CAMPER_TYPE_2) {
        this.CAMPER_TYPE_2 = CAMPER_TYPE_2;
    }

    /**
     * @return the CAMPER_TYPE_3
     */
    public int getCAMPER_TYPE_3() {
        return CAMPER_TYPE_3;
    }

    /**
     * @param CAMPER_TYPE_3 the CAMPER_TYPE_3 to set
     */
    public void setCAMPER_TYPE_3(int CAMPER_TYPE_3) {
        this.CAMPER_TYPE_3 = CAMPER_TYPE_3;
    }

    private int CAMPER_TYPE_1 = 20;
    private int CAMPER_TYPE_2 = 15;
    private int CAMPER_TYPE_3 = 12;

    private int camperType;

    public static ArrayList<Camper> loadCampers(CamperRepository cr) {
        ArrayList<Camper> campers = (ArrayList<Camper>) cr.findAll();
        HashMap<Integer, CamperType> camperTypesMap = CamperTypeBO.getCamperTypesMap();
        for (Camper current : campers) {
            if (current.getCamperType() != null) {
                current.setCamperType(current.getCamperType());
            }
        }
        return campers;

    }

    /**
     * Connect to the data access object to get the campers from the datasource.
     *
     * @since 20200923
     * @author BJM
     */
    public ArrayList<Camper> load() {

        //Read campers from the database
        CamperDAO camperDAO = new CamperDAO();
        ArrayList<Camper> campers = camperDAO.select();

        return campers;
    }

    public Camper save(Camper camper) {
        CamperDAO camperDAO = new CamperDAO();

        //NOTE:  The id attribute generated from the database is an Integer not an
        //       int.  The default for an Integer is null so can't compare to 0.
        if (camper.getId() == null) {
            camper = camperDAO.insert(camper);
        } else {
            camperDAO.update(camper);
        }

        return camper;
    }

    /**
     * Delete the camper
     *
     * @since 20201009
     * @author BJM
     */
    public boolean delete(int id) {
        CamperDAO camperDAO = new CamperDAO();
        return camperDAO.delete(id);
    }

    /**
     * Calculate the camper food cost based on the selected type. This method is
     * also used for testing in the CamperBOJUnitTest class
     *
     * @since 20201210
     * @author K.Taylor
     *
     */
    public int getFoodCost(int camperType) {

        if (camperType == 1) {
            camperType = 20;
        } else if (camperType == 2) {
            camperType = 15;
        } else if (camperType == 3) {
            camperType = 12;
        }

        return camperType;

    }

}
