package info.hccis.camper.dao;

import info.hccis.camper.bo.CamperBO;
import info.hccis.camper.jpa.entity.Camper;
import info.hccis.camper.jpa.entity.CamperType;
import info.hccis.camper.util.DatabaseUtility;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Camper database access object
 *
 * @author K.Taylor
 * @since 20201210
 */
public class CamperDAO {

    /**
     * Select the records from the database
     *
     * @since 20200923
     * @author BJM
     */
    public ArrayList<Camper> select() {
        ArrayList<Camper> campers = new ArrayList();

        ArrayList<CamperType> camperTypes = new ArrayList();
        CamperTypeDAO camperTypeDAO = new CamperTypeDAO();
        camperTypes = camperTypeDAO.select();
        
        Connection conn = null;
        try {

            //Select the campers from the database
            conn = DatabaseUtility.getConnection("");
            Statement statement = conn.createStatement();

            //***************************************************
            // Select using statement
            //***************************************************
            //Next select all the rows and display them here...
            ResultSet rs = statement.executeQuery("select * from Camper");

            //Show all the campers
            while (rs.next()) {
                int id = rs.getInt("id");
                String firstName = rs.getString("firstName");
                String lastName = rs.getString("lastName");
                String dob = rs.getString("dob");
                Integer camperType = rs.getInt("camperType");
                
                Camper camper = new Camper(id);
                camper.setFirstName(firstName);
                camper.setLastName(lastName);
                camper.setDob(dob);
                camper.setCamperType(camperType);
                
               
                
                for(CamperType current: camperTypes){
                    if(current.getId().equals(camperType) ){
                         CamperBO camperBO = new CamperBO();
                        camper.setCamperType(camperBO.getCamperType());
                    }
                }
                
                campers.add(camper);

            }

        } catch (SQLException ex) {
            Logger.getLogger(CamperDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                conn.close();
            } catch (SQLException ex) {
                //Could not close.  
                System.out.println("Error closing the connection.");
            }
        }
        return campers;

    }

    /**
     * Insert the camper into the database.
     *
     * @since 20201210
     * @author K.Taylor
     *
     * 20200925 BJM Modifications to get running using standard password.
     */
    public Camper insert(Camper camper) {

        Connection conn = null;
        CamperBO camperBO = new CamperBO();
        
        

        //***************************************************
        // INSERT
        //***************************************************
        try {
            conn = DatabaseUtility.getConnection("");
            String theStatement = "INSERT INTO Camper(firstName,lastName,dob, camperType) "
                    + "VALUES (?,?,?,?)";
            PreparedStatement stmt = conn.prepareStatement(theStatement);
            stmt.setString(1, camper.getFirstName());
            stmt.setString(2, camper.getLastName());
            stmt.setString(3, camper.getDob());
            
            //Inserts a camper type into the new field
            stmt.setDouble(4, camper.getCamperType());
            
           //Saves the camper food cost based on the selected type before saving it to the database
            camperBO.getFoodCost(camper.getCamperType());

            stmt.executeUpdate();

        } catch (SQLException sqle) {
            System.out.println("sql exception caught");
            sqle.printStackTrace();
        }

        return camper;
    }

     /**
     * Update the camper into the database.
     *
     * @since 20201009
     * @author BJM
     *
     */
    public Camper update(Camper camper) {

        Connection conn = null;
          CamperBO camperBO = new CamperBO();

        //***************************************************
        // INSERT
        //***************************************************
        try {
            conn = DatabaseUtility.getConnection("");
            String theStatement = "UPDATE Camper set firstName=?,lastName=?,dob=?, camperType=? "
                    + "WHERE id = ?";
            PreparedStatement stmt = conn.prepareStatement(theStatement);
            stmt.setString(1, camper.getFirstName());
            stmt.setString(2, camper.getLastName());
            stmt.setString(3, camper.getDob());
            stmt.setDouble(4, camper.getCamperType());

            stmt.setInt(5, camper.getId());
            camperBO.getFoodCost(camper.getCamperType());
            stmt.executeUpdate();

        } catch (SQLException sqle) {
            System.out.println("sql exception caught");
            sqle.printStackTrace();
        }

        return camper;
    }

     /**
     * Update the camper into the database.
     *
     * @since 20201009
     * @author BJM
     *
     */
    public boolean delete(int id) {

        Connection conn = null;

        //***************************************************
        // INSERT
        //***************************************************
        try {
            conn = DatabaseUtility.getConnection("");
            String theStatement = "DELETE FROM Camper WHERE id = ?";
            PreparedStatement stmt = conn.prepareStatement(theStatement);
            stmt.setInt(1, id);

            stmt.executeUpdate();

        } catch (SQLException sqle) {
            System.out.println("sql exception caught");
            sqle.printStackTrace();
            return false;
        }

        return true;
    }
    
}
