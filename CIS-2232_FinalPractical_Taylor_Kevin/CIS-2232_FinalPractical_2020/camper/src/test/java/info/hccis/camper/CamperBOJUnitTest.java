package info.hccis.camper;

import info.hccis.camper.bo.CamperBO;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author Kevin
 */
public class CamperBOJUnitTest {

    public CamperBOJUnitTest() {
    }

    @BeforeAll
    public static void setUpClass() {
    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {
    }

    @AfterEach
    public void tearDown() {
    }

    /**
     * Test the calculate method from the CamperBO class.
     *
     * @since 20201210
     * @author K.Taylor
     *
     */
    @Test
    public void testGetCamperType1FoodCost() {
        CamperBO camperBO = new CamperBO();
        camperBO.setCamperType(1);
        int total = camperBO.getFoodCost(camperBO.getCamperType());
        Assertions.assertEquals(camperBO.getCAMPER_TYPE_1(), total);

    }

    /**
     * Test the calculate method from the CamperBO class.
     *
     * @since 20201210
     * @author K.Taylor
     *
     */
    @Test
    public void testGetCamperType2FoodCost() {
        CamperBO camperBO = new CamperBO();
        camperBO.setCamperType(2);
        int total = camperBO.getFoodCost(camperBO.getCamperType());
        Assertions.assertEquals(camperBO.getCAMPER_TYPE_2(), total);

    }

}
